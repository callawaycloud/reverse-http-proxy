const fs = require('fs');
const httpProxy = require('http-proxy');

const proxy = httpProxy.createServer({
  target: {
    host: process.env.HOST || 'localhost',
    port: process.env.PORT || 5100,
  },
  ssl: {
    cert: fs.readFileSync('cert.pem', 'utf8'),
    key: fs.readFileSync('key.pem', 'utf8')
  },
  function (e) {
    console.log(e)
  }
}).listen(443);

proxy.on('error', function (err, req, res) {
    res.writeHead(500, {
        'Content-Type': 'text/plain'
    });
    res.end('Something went wrong');
});

console.log("I'm alive!");